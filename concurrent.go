package main 

import ( 
  "encoding/json"
  "log"
  "os"
  "strconv"
  "time"
)

type Device struct { 
  Name string `json:"Name`
  Id  int `json:"id"`
}

func main() {
  var device Device 

  file, err := os.Create("data.json") 
  if err != nil { 
    log.Fatal(err)
  }

  defer file.Close()  
  
  start := time.Now() 


  for i := 0; i <= 1e5; i++ { 
  
    go func(i int) { 
      defer close(chan int) 

      device.Name = "name " + strconv.Itoa(i)
      device.Id = i


      encoder := json.NewEncoder(file)
      err = encoder.Encode(device) 

      if err != nil { 
       log.Fatal(err)
      }
    }(i)
  }

  log.Println("Success", time.Since(start)) 
}
