package main 

import ( 
  "encoding/json"
  "log"
  "os"
  "strconv"
  "time"
  "sync"
)

type Device struct { 
  Name string `json:"Name`
  Id  int `json:"id"`
}

func main() {
  var device Device 

  file, err := os.Create("data.json") 
  if err != nil { 
    log.Fatal(err)
  }

  defer file.Close()  
  
  start := time.Now() 

  var wg sync.WaitGroup
  var mutex sync.Mutex

  for i := 0; i <= 1e6; i++ { 
    wg.Add(1) // increment number of goroutines
  
    go func(i int) { 
      defer wg.Done()

      device.Name = "name " + strconv.Itoa(i)
      device.Id = i

      mutex.Lock()
      defer mutex.Unlock()

      encoder := json.NewEncoder(file)
      err = encoder.Encode(device) 

      if err != nil { 
       log.Fatal(err)
      }
    }(i) 
  }

  wg.Wait()
  log.Println("Success", time.Since(start)) 
}
