package main 

import ( 
  "encoding/json"
  "log"
  "os"
  "strconv"
  "time"
)

type Device struct { 
 // tag struct  
  Name string `json:"name"`
  Id  int `json:"id"`
}

func main() {
  var device Device 

  file, err := os.Create("data.json") 
  if err != nil { 
    log.Fatal(err)
  }

  defer file.Close()  
  
  start := time.Now() 

  for i := 0; i <= 1e6; i++ { 
    device.Name = "name " + strconv.Itoa(i)
    device.Id = i

    encoder := json.NewEncoder(file)
    err = encoder.Encode(device) 

    if err != nil { 
      log.Fatal(err)
    }
  }

  log.Println("Success", time.Since(start)) 
}
